#!/bin/bash

# Function to test a file for malicious content
test_file() {
    file_name="$1"

    # Checking if the file name contains any of the specified dangerous words
    dangerous_words=("corrupted" "dangerous" "risk" "attack" "malware" "malicious")
    for word in "${dangerous_words[@]}"; do
        if grep -q -i "$word" "$file_name"; then
            echo "File $file_name is considered suspicious (contains word: $word)."
            exit 0
        fi
    done

    # Checking for non-ASCII characters in the file name
    if [[ "$(echo "$file_name" | LC_ALL=C grep -P '[^\x00-\x7F]')" ]]; then
        echo "File $file_name is considered suspicious (contains non-ASCII characters)."
        exit 0
    fi

    # Checking if the file contains fewer than 3 lines but more than 2000 characters
    line_count=$(wc -l < "$file_name")
    char_count=$(wc -m < "$file_name")
    if (( line_count < 3 )) && (( char_count > 2000 )); then
        echo "File $file_name is considered suspicious (fewer than 3 lines but more than 2000 characters)."
        exit 0
    fi

    echo "$file_name"
    exit 1
}

# Calling the test_file function with the file path as argument
test_file "$1"