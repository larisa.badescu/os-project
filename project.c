#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include <sys/wait.h>    // for waitpid()

// headers for handling file system operations and directory traversal in Linux
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

#define MAX 10

// for visually formatting the output of the directory structure
void printing(int dim, FILE* file) {
    for (int l = 0; l < dim; ++l) {
        fprintf(file, "|  ");
    }
}

// Move file to the isolated space directory
void moveFile(char* filePath, char* isolatedSpaceDir) {
    char* fileName = strrchr(filePath, '/') + 1; // Extracting file name from the path
    char destPath[1024];
    snprintf(destPath, sizeof(destPath), "%s/%s", isolatedSpaceDir, fileName);
    if (rename(filePath, destPath) != 0) {
        perror("Error moving file to isolated space directory");
    } else {
        printf("File %s moved to isolated space directory.\n", fileName);
    }
}

/* function responsible for recursively traversing directories.
It takes the path of the directory to parse and the current depth in the directory tree as arguments */
void parseFolder(char* folderPath, int depth, FILE* file, char* izolated_space_dir) {
    DIR* parent = opendir(folderPath);                      // Opening the directory
    if (parent == NULL) {
        perror("Error opening the directory");
        return;
    }

    struct dirent* entry;
    char path[1024];
    fprintf(file, "%s\n", folderPath);                     // Reading each entry in the directory and printing the path of the current directory being processed
    int maliciousFiles = 0;                                // Counter for corrupted files
    while ((entry = readdir(parent)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)    // Processing directory entries & skipping the current(".") and parent("..") directory entries
        {
            continue;
        }

        snprintf(path, sizeof(path), "%s/%s", folderPath, entry->d_name);           // Constructing the full path

        // Retrieving the status of the file
        struct stat statbuffer;
        if (stat(path, &statbuffer) == -1) {
            perror("Error trying to get the file status");
            continue;
        }
        if (S_ISDIR(statbuffer.st_mode)) {
            printing(depth, file);
            fprintf(file, "├── %s (%ld bytes)\n", entry->d_name, (long)statbuffer.st_size);
            parseFolder(path, depth + 1, file, izolated_space_dir);
        } else   // if the entry is a file, it prints its name, size, last modification time, permissions, and inode number
        {
            printing(depth, file);
            fprintf(file, "├── %s (%ld bytes)\n", entry->d_name, (long)statbuffer.st_size);
            fprintf(file, "│   Last Modified: %s", ctime(&statbuffer.st_mtime));
            fprintf(file, "│   Permissions: ");  
            fprintf(file, (statbuffer.st_mode & S_IRUSR) ? "r" : "-");
            fprintf(file, (statbuffer.st_mode & S_IWUSR) ? "w" : "-");
            fprintf(file, (statbuffer.st_mode & S_IXUSR) ? "x" : "-");
            fprintf(file, (statbuffer.st_mode & S_IRGRP) ? "r" : "-");
            fprintf(file, (statbuffer.st_mode & S_IWGRP) ? "w" : "-");
            fprintf(file, (statbuffer.st_mode & S_IXGRP) ? "x" : "-");
            fprintf(file, (statbuffer.st_mode & S_IROTH) ? "r" : "-");
            fprintf(file, (statbuffer.st_mode & S_IWOTH) ? "w" : "-");
            fprintf(file, (statbuffer.st_mode & S_IXOTH) ? "x" : "-");
            fprintf(file, "\n");
            fprintf(file, "│   inode no: %ld\n", (long)statbuffer.st_ino);

            // Checking if all permissions are missing
            if ((statbuffer.st_mode & S_IRWXU) == 0 && (statbuffer.st_mode & S_IRWXG) == 0 && (statbuffer.st_mode & S_IRWXO) == 0) {
                //pipe for communication between child and parent
                int pipefd[2];
                if (pipe(pipefd) == -1) {
                    perror("Error trying to create pipe");
                    return;
                }

                pid_t pid = fork();             // to create a new process (two identical processes are created: the parent process and the child process)
                if (pid == -1) {
                    perror("Error creating child process");
                    return;
                } else if (pid == 0){                              // Child process
                    close(pipefd[0]);                              // Close unused read end
                    dup2(pipefd[1], STDOUT_FILENO);                // Redirect stdout to pipe
                    close(pipefd[1]);                              // Close write end of pipe

                    // Executing the script to test the file
                    char* script_args[] = {"./script.sh", path, NULL};
                    execvp(script_args[0], script_args);
                    perror("Error executing script");
                    exit(1);
                } else {
                    close(pipefd[1]);                               // Close unused write end
                    char buffer[1024];
                    int nbytes = read(pipefd[0], buffer, sizeof(buffer));
                    if (nbytes <= 0) {
                        printf("Error trying to read from the pipe\n");
                    } else {
                        buffer[nbytes] = '\0';
                        if (strcmp(buffer, "SAFE\n") == 0) {
                            printf("File %s is safe.\n", entry->d_name);
                        } else {
                            printf("File %s is dangerous.\n", entry->d_name);
                            maliciousFiles++;
                            // Move corrupted file to isolated space directory
                            moveFile(path, izolated_space_dir);
                        }
                    }
                    close(pipefd[0]); // Close read end of pipe
                }
            }
        }
    }
    printf("Child process has ended with PID %d and %d files with potential danger.\n", getpid(), maliciousFiles);
    closedir(parent);
}

// function responsible for creating a snapshot of a directory
void updatedSnap(char* dir_path, char* output_dir, char* izolated_space_dir, int process_number) {
    char snapFilePath[1024];

    time_t curr_time;
    struct tm* timeINFO;
    time(&curr_time);                   // get current time
    timeINFO = localtime(&curr_time);   // converts the current time stored in curr_time into local time

    snprintf(snapFilePath, sizeof(snapFilePath), "%s/Snapshot_%d_%s.txt", output_dir, process_number, dir_path);

    FILE* snap_file = fopen(snapFilePath, "w");
    if (snap_file == NULL) {
        perror("Error when opening the snapshot file:(");
        exit(2);
    }

    fprintf(snap_file, "Timestamp: %s", asctime(timeINFO));
    fprintf(snap_file, "Folder: %s\n", dir_path);
    fprintf(snap_file, "\nContent of folder:\n");
    parseFolder(dir_path, 1, snap_file, izolated_space_dir);

    fclose(snap_file);
    exit(0);                         // Exiting the child process after creating the snapshot
}

/* here we parse command-line arguments, check the validity of the input, create the output directory if it doesn't exist,
and then take a snapshot for each specified directory */
int main(int argc, char** argv) {
    if ((argc < 4) || (argc > 13)) {
        fprintf(stderr, "Not the right number of arguments:(\n");
        return 1;
    }

    char* output_dir=NULL;
    char* izolated_space_dir=NULL;
    char* directories[MAX];
    int nr_dir=0;

    for (int i = 1; i < argc; i++) {                // iterating through the command line arguments
        if (strcmp(argv[i], "-o") == 0) {
            i++;
            output_dir = argv[i];
        } else if (strcmp(argv[i], "-s") == 0) {
            i++;
            izolated_space_dir = argv[i];
        } else {
            if (nr_dir >= MAX) {
                fprintf(stderr, "Too many directories in the terminal - we can't have more than 10\n");
                return 1;
            }
            directories[nr_dir++] = argv[i];
        }
    }

    if (output_dir == NULL) {
        fprintf(stderr, "The output directory is not specified, try again:)\n");
        return 1;
    }

    struct stat st;
    if (stat(output_dir, &st) == 0 && S_ISDIR(st.st_mode))      // checks if the output directory exists and is a directory
    {
        printf("Output directory: '%s' exists and snapshots will be stored there:) \n", output_dir);
    } else {
        if (mkdir(output_dir, 0777) == -1) {
            perror("Error when trying to create output directory");
            return 1;
        }
    }

    if (izolated_space_dir == NULL) {
        fprintf(stderr, "The isolated space directory is not specified, try again:)\n");
        return 1;
    }

    // Creating child processes for each directory
    pid_t PIDchildren[MAX];
    for (int i = 0; i < nr_dir; i++) {
        pid_t pid = fork();
        if (pid == -1) {
            perror("Error trying to create child process");
            return 1;
        } else if (pid == 0)            // Child process
        {
            updatedSnap(directories[i], output_dir, izolated_space_dir, i + 1);
        } else                          // Parent process
        {
            PIDchildren[i] = pid;       // Store child PID in array
        }
    }

    // Parent process waiting for all child processes to finish
    int status;
    for (int i = 0; i < nr_dir; i++) {
        waitpid(PIDchildren[i], &status, 0);
        if (WIFEXITED(status))                  // Child process terminated normally
        {
            printf("Snapshot for Directory %d created successfully. Child process %d terminated with PID %d and exit code %d.\n", i + 1, i + 1, PIDchildren[i], WEXITSTATUS(status));
        } else                                  // Child process terminated abnormally
        {
            printf("Snapshot for Directory %d failed. Child process %d terminated abnormally with PID %d.\n", i + 1, i + 1, PIDchildren[i]);
        }
    }

    return 0;
}